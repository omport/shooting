﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public float movingSpeed = 1.0f;
    GameObject player;
    // PlayerBulletプレハブ
    public GameObject bullet;
    public float BulletCreateSpeed = 1.0f;
    public Camera _setCamera;

    // Use this for initialization
    void Start () {

        //自機のゲームオブジェクトの取得
        player = GameObject.Find("Player");
        _setCamera = Camera.main;

    }
	// Update is called once per frame
	void Update () {

        //キー入力
        Input_key();

        //弾を発射する
        ShotBullet();

    }
    // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D(Collider2D c)
    {
        //ぶつかった相手を削除
        Destroy(c.gameObject);

        //プレイヤーを削除
        Destroy(player);
    }
    //キー入力の関数
    void Input_key()
    {
        float vertical = Input.GetAxis("Vertical");

        float horizontal = Input.GetAxis("Horizontal");

        Vector3 positionInScreen = _setCamera.WorldToViewportPoint(transform.position);

        if (positionInScreen.y <= 1)
        {
            if (Input.GetKey("up"))
            {
                transform.Translate(0, vertical * movingSpeed, 0);
            }
        }
        if (positionInScreen.y >= 0)
        {
            if (Input.GetKey("down"))
            {
                transform.Translate(0, vertical * movingSpeed, 0);
            }
        }
        if (positionInScreen.x >= 0)
        {
            if (Input.GetKey("left"))
            {
                transform.Translate(horizontal * movingSpeed, 0, 0);
            }
        }
        if (positionInScreen.x <= 1)
        {
            if (Input.GetKey("right"))
            {
                transform.Translate(horizontal * movingSpeed, 0, 0);
            }
        }
    }
    //弾を発射する関数
    void ShotBullet()
    {
        if (Input.GetKeyDown("z"))
        {
            StartCoroutine("CreateBullet");

        }
        else if(Input.GetKeyUp("z"))
        {
            StopCoroutine("CreateBullet");
        }
    }
    //弾を生成する関数
    IEnumerator CreateBullet()
    {
        while(true)
        {
            // 弾をプレイヤーと同じ位置/角度で作成
            Instantiate(bullet, transform.position, transform.rotation);
            yield return new WaitForSeconds(BulletCreateSpeed);
        }
    }
}
