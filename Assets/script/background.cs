﻿using UnityEngine;
using System.Collections;

public class background : MonoBehaviour {

    // スクロールするスピード
    public float speed = 1.0f;
    public float max_limit = 1.0f;
    private float first_position = 1.0f;

    Vector3 offset;

    // Use this for initialization
    void Start () {
        first_position = transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {

        // 時間によってYの値が0から上限値に変化していく。上限値になったら0に戻り、繰り返す。
        float y = Mathf.Repeat(Time.time * speed, max_limit);

        // Yの値がずれていくオフセットを作成
        offset = new Vector3(0, first_position + y, 0);

        // マテリアルにオフセットを設定する
        transform.position = offset;
    }
}
