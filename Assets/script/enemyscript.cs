﻿using UnityEngine;
using System.Collections;

public class enemyscript : MonoBehaviour {

    public GameObject bullet;
    public float BulletCreateSpeed = 1.0f;
    GameObject enemy;

    // Use this for initialization
    void Start () {

        StartCoroutine("CreateBullet");
        //自機のゲームオブジェクトの取得
        enemy = GameObject.Find("enemy1");
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    // ぶつかった瞬間に呼び出される
    void OnTriggerEnter2D(Collider2D c)
    {
        //ぶつかった相手を削除
        Destroy(c.gameObject);

        //自分を削除
        Destroy(enemy);
    }
    //弾を生成する関数
    IEnumerator CreateBullet()
    {
        while (true)
        {
            // 弾をプレイヤーと同じ位置/角度で作成
            Instantiate(bullet, transform.position, transform.rotation);
            yield return new WaitForSeconds(BulletCreateSpeed);
        }
    }
}
